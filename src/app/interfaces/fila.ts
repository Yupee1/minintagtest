export class Fila {
  indice: number = 0;
  numero: number = 0;
  ocurrencias: number = 0;
  fpos: number = 0;
  lpos: number = 0;
  color: string = 'bg-warning';
}
