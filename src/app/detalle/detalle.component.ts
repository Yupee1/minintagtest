import { Component, OnInit } from '@angular/core';
import { Detalle } from '../interfaces/detalle';
import { Fila } from '../interfaces/fila';
import { FilaDictado } from '../interfaces/FilaDictado';
import { DetalleService } from './detalle.service';


@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.css']
})
export class DetalleComponent implements OnInit {

  detalle: Detalle;
  filteredDetalle: Fila[];
  ordenado: string = '';
  mostrarArray: string = '';
  cabeceras: any[];
  asci: number[] = [];
  filteredDictado: FilaDictado[] = [];

  constructor(private detalleService: DetalleService) { }

  ngOnInit(): void {
    for (let i = 31; i < 127; i++) {
      this.asci[i] = i;
    }
  }

  consumeEndpoint() {
    this.mostrarArray = 'array';
    this.detalleService.getArray().subscribe((response) => {
      this.detalle = response;
      this.filteredDetalle = this.enchularListado(this.detalle.data);
      this.initOrdenado();
    });
  }
  consumeEndpoint2() {
    this.mostrarArray = 'dictado';
    this.crearHeaders();
    this.detalleService.getDictado().subscribe((response) => {
      this.detalle = { "data": JSON.parse(response.data), "error": response.error, "success": response.success };
      this.filteredDictado = this.enchularDictado(this.detalle.data);
    });
  }
  enchularListado(listado: any[]): Fila[] {
    let numeros: Fila[] = [];
    let listaOriginal = listado;

    for (let i = 0; i < listaOriginal.length; i++) {
      let { ocurrencias, firstPos, lastPos } = this.detalleDatFiltered(listado, listaOriginal[i]);

      let color = 'bg-warning';
      if (ocurrencias < 1) {
        color = 'bg-dark';
      } else if (ocurrencias >= 2) {
        color = 'bg-success';
      }
      numeros[i] = { indice: i, numero: listaOriginal[i], ocurrencias: ocurrencias, fpos: firstPos, lpos: lastPos, color };
    }
    return numeros;
  }

  /**
   * retorna 3 valores la cantidad de veces que se repite, primera posicion del numero, ultimo index del numero que se repite
   * @param num numero de cada fila de data del endpoint
   */
  detalleDatFiltered(listado: any[], num: number): { ocurrencias, firstPos, lastPos } {

    let ocurrencias = 0;
    let firstPos = 0;
    let lastPos = 0;

    if (!!this.detalle.data) {

      let numeros = listado;
      for (let i = 0; i < numeros.length; i++) {
        if (num === numeros[i]) {
          //cuando el numero coincida con el del arreglo ,ocurrencias aumenta en 1
          ocurrencias++;
          //cuando coincida el numero al del array tomare el indice que corresnponde al indicide de su ultima repeticion
          lastPos = i;
          //si ocurrencias es 1 es porque ya hay un valor de este numero y corresponde al primero por en
          if (ocurrencias == 1) {
            firstPos = i;
          }
        }
      }
    }
    return { ocurrencias, firstPos, lastPos };
  }

  initOrdenado() {
    let lista = this.detalle.data;
    for (let i = 0; i < lista.length - 1; i++) {
      // en peroceso loading modem sound
      // let minimo = i;
      // for (let j = i + 1; lista.length; j++) {
      //   if (lista[j] < lista[minimo]) {
      //     minimo = j;
      //   }
      // }
      // let listaAux = lista[i];
      // lista[i] = lista[minimo];
      // lista[minimo] = listaAux;
      this.ordenado = this.ordenado + lista[i] + ' ';
    }
  }

  crearHeaders() {
    this.cabeceras = [];
    //genera columnas de A - Z utilizando codigo ascii
    for (let i = 97, j = 0; i <= 122; i++, j++) {
      this.cabeceras[j] = String.fromCharCode(i);
    }
  }
  enchularDictado(listado: any[]): any[] {
    let dictados: { paragraph: string, number: number, hasCopyright: boolean }[] = listado;
    //este es el metodo que listara la tabla de dictados y es de tipo FilaDictado
    let ocurrenciasAbc: FilaDictado[] = [];
    dictados.forEach((dictado: { paragraph: string, number: number, hasCopyright: boolean }) => {
      let arrDictado = [];
      let index = dictados.indexOf(dictado);
      ocurrenciasAbc[index] = new FilaDictado();
      ocurrenciasAbc[index].indice = index;
      ocurrenciasAbc[index].parrafo = dictado.paragraph;
      //recorro el array de data y tomo el parrafo
      for (let i = 0; i < dictado.paragraph.length; i++) {
        //recorro el parrafo en si para tomar cada letra del texto
        for (let j = 0; j < dictado.paragraph[i].length; j++) {
          arrDictado[i] = dictado.paragraph[i].toLowerCase().charCodeAt(j);
          //hago un for de las letras a -z y pregunto si la letra del parafo es igual a una letra si lo es en mi array de letras de FilaDictado.abc aumenta en + para cada letra
          //k equivale al ascii de cada letra
          for (let k = 97, l: number = 0; k <= 122; k++, l++) {
            if (isNaN(ocurrenciasAbc[index].abc[l])) {
              ocurrenciasAbc[index].abc[l] = 0;
            }
            ocurrenciasAbc[index].abc[l] += 0;
            if (k == arrDictado[i]) {
              ocurrenciasAbc[index].abc[l]++;
              break;
            }
          }
        }
      }
    });
    return ocurrenciasAbc;
  }
}
