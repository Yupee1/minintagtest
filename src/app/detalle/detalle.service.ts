import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Detalle } from '../interfaces/detalle';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DetalleService {

  constructor(private http: HttpClient) { }

  public getArray() {
    return this.http.get<Detalle>(`${environment.apiUrl}array.php`);
  }
  public getDictado() {
    return this.http.get<Detalle>(`${environment.apiUrl}dict.php`);
  }
}
